fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
// .then(json => {json.map((elem) => {console.log(elem.title) })})
.then(json => {let arrayNew = json.map((elem) => {
	return elem.title});
	console.log(arrayNew)
})


// GET 1 
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json))

//Template Literal
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item ${json.title} has a status of ${json.completed}`))


// POST
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: 'POST',
	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		completed: false,
		id: 201,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// PUT
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
	
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 201,
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1

	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// PATCH
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,
		status: "Pending",
		userId: 1
	})
})
.then((response) => response.json())
// .then((json) => console.log(json));

// PATCH
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
	
		dateCompleted:"07/09/21",
		status: "Completed"

	})
})

.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'DELETE',
})